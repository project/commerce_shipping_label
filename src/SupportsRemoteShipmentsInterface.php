<?php

namespace Drupal\commerce_shipping_label;

use Drupal\commerce_shipping\Entity\ShipmentInterface;

interface SupportsRemoteShipmentsInterface {

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \Drupal\commerce_shipping_label\RemoteShipment|null
   */
  public function createRemoteShipment(ShipmentInterface $shipment) : ?RemoteShipment;

  /**
   * @param string $id
   *
   * @return \Drupal\commerce_shipping_label\RemoteShipment|null
   */
  public function getRemoteShipment(string $id) : ?RemoteShipment;

  /**
   * @param string $id
   */
  public function cancelRemoteShipment(string $id);

}
