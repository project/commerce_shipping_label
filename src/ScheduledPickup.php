<?php

namespace Drupal\commerce_shipping_label;

use Drupal\commerce_shipping\Entity\ShipmentInterface;

class ScheduledPickup {

  public const STATUS_UNKNOWN = 'unknown';

  public const STATUS_SCHEDULED = 'scheduled';

  public const STATUS_CANCELED = 'canceled';

  protected ?string $remoteId;

  protected ShipmentInterface $shipment;

  protected string $status;

  protected ?string $confirmationNumber;

  /**
   * @var \Drupal\commerce_schedule_pickup\ScheduledPickupRate[]
   */
  protected array $rates;

  /**
   * @var array $data
   */
  protected array $data;

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   */
  public function __construct(ShipmentInterface $shipment) {
    $this->shipment = $shipment;
    $this->rates = [];
    $this->data = [];
    $this->status = self::STATUS_UNKNOWN;
  }

  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

  public function setShipment(ShipmentInterface $shipment): ScheduledPickup {
    $this->shipment = $shipment;
    return $this;
  }

  public function getStatus(): string {
    return $this->status;
  }

  public function setStatus(string $status): ScheduledPickup {
    $this->status = $status;
    return $this;
  }

  public function getConfirmationNumber(): ?string {
    return $this->confirmationNumber;
  }

  public function setConfirmationNumber(?string $confirmationNumber): ScheduledPickup {
    $this->confirmationNumber = $confirmationNumber;
    return $this;
  }

  public function getRates(): array {
    return $this->rates;
  }

  /**
   * @param \Drupal\commerce_schedule_pickup\ScheduledPickupRate[] $rates
   *
   * @return $this
   */
  public function setRates(array $rates): ScheduledPickup {
    $this->rates = $rates;
    return $this;
  }

  public function addRate(ScheduledPickupRate $rate): ScheduledPickup {
    $this->rates[] = $rate;
    return $this;
  }

  public function getRemoteId(): ?string {
    return $this->remoteId;
  }

  public function setRemoteId(?string $remoteId): ScheduledPickup {
    $this->remoteId = $remoteId;
    return $this;
  }

  public function getData(): array {
    return $this->data;
  }

  public function setData(array $data): ScheduledPickup {
    $this->data = $data;
    return $this;
  }

  public function setDataValue($name, $value) : ScheduledPickup {
    $this->data[$name] = $value;
    return $this;
  }

  public function getDataValue($name) {
    return $this->data[$name] ?? NULL;
  }

}

