<?php

namespace Drupal\commerce_shipping_label;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping_label\Event\RemoteShipmentEvent;
use Drupal\commerce_shipping_label\Event\ShippingLabelEvent;
use Drupal\commerce_shipping_label\Event\ShippingLabelEvents;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ShippingLabelManager {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * ShippingLabelManager constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   */
  public function __construct(FileSystemInterface $fileSystem, EventDispatcherInterface $eventDispatcher) {
    $this->fileSystem = $fileSystem;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return string|null
   */
  public function getRemoteShipmentId(ShipmentInterface $shipment) : ?string {
    if ($shipment->hasField('shipment_label_remote_id') && !$shipment->get('shipment_label_remote_id')->isEmpty()) {
      return $shipment->get('shipment_label_remote_id')->value;
    }
    return NULL;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return bool
   */
  public function hasRemoteId(ShipmentInterface $shipment) : bool {
    return $this->getRemoteShipmentId($shipment) !== NULL;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return bool
   */
  public function supportsRemoteShipments(ShipmentInterface $shipment) : bool {
    $shipping_method = $shipment->getShippingMethod();
    $plugin = ($shipping_method !== NULL) ? $shipping_method->getPlugin() : NULL;
    return $plugin !== NULL && $plugin instanceof SupportsRemoteShipmentsInterface;
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return bool
   */
  public function supportsSchedulingPickups(ShipmentInterface $shipment) : bool {
    $shipping_method = $shipment->getShippingMethod();
    $plugin = ($shipping_method !== NULL) ? $shipping_method->getPlugin() : NULL;
    return $plugin !== NULL && $plugin instanceof SupportsSchedulingPickup;
  }

  public function createRemoteShipment(ShipmentInterface $shipment) {
    $shipping_method = $shipment->getShippingMethod();
    $plugin = ($shipping_method !== NULL) ? $shipping_method->getPlugin() : NULL;
    if ($plugin !== NULL && $plugin instanceof SupportsRemoteShipmentsInterface) {
      $this->eventDispatcher->dispatch(ShippingLabelEvents::REMOTE_SHIPMENT_PRECREATE, new RemoteShipmentEvent($shipment));
      $remote_shipment = $plugin->createRemoteShipment($shipment);
      $this->eventDispatcher->dispatch(ShippingLabelEvents::REMOTE_SHIPMENT_CREATED, new RemoteShipmentEvent($shipment, $remote_shipment));
      // Data will be saved in event subscriber.
    }
  }

  public function cancelRemoteShipment(ShipmentInterface $shipment) : bool {
    $cancelled = FALSE;
    $shipping_method = $shipment->getShippingMethod();
    $plugin = ($shipping_method !== NULL) ? $shipping_method->getPlugin() : NULL;
    if ($plugin !== NULL && $plugin instanceof SupportsRemoteShipmentsInterface) {
      try {
        $this->eventDispatcher->dispatch(ShippingLabelEvents::REMOTE_SHIPMENT_PRECANCEL, new RemoteShipmentEvent($shipment));
        $cancelled = $plugin->cancelRemoteShipment($this->getRemoteShipmentId($shipment));
        $this->eventDispatcher->dispatch(ShippingLabelEvents::REMOTE_SHIPMENT_CANCELLED, new RemoteShipmentEvent($shipment));
      }
      catch (\Exception | \Error $e) {
        throw new ShippingLabelGenerationException($e->getMessage(), $e->getCode(), $e);
      }
      // Data will be saved in event subscriber.
    }
    return $cancelled;
  }

  public function deleteLabelFiles(ShipmentInterface $shipment) {
    $shipping_method = $shipment->getShippingMethod();
    $plugin = ($shipping_method !== NULL) ? $shipping_method->getPlugin() : NULL;
    if ($plugin !== NULL && $plugin instanceof SupportsImportingShippingLabelsInterface) {
      try {
        $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LABEL_PREDELETE, new ShippingLabelEvent($shipment));
        $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LABEL_DELETED, new ShippingLabelEvent($shipment));
        // Files will be deleted in event subscriber.
      }
      catch (\Exception | \Error $e) {
        throw new ShippingLabelGenerationException($e->getMessage(), $e->getCode(), $e);
      }
    }
  }

  public function importLabelFiles(ShipmentInterface $shipment) {
    $shipping_method = $shipment->getShippingMethod();
    $plugin = ($shipping_method !== NULL) ? $shipping_method->getPlugin() : NULL;
    if ($plugin !== NULL && $plugin instanceof SupportsImportingShippingLabelsInterface) {
      $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LABEL_PREIMPORT, new ShippingLabelEvent($shipment));
      $remote_label_files = $plugin->getRemoteLabelFiles($shipment);
      $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LABEL_IMPORTED, new ShippingLabelEvent($shipment, $remote_label_files));
      // Files will be saved in event subscriber.
    }
  }


  public function cancelPickup(ShipmentInterface $shipment) : bool {
    $cancelled = FALSE;
    $shipping_method = $shipment->getShippingMethod();
    $plugin = ($shipping_method !== NULL) ? $shipping_method->getPlugin() : NULL;
    if ($plugin !== NULL && $plugin instanceof SupportsSchedulingPickup && $pickup_remote_id = $shipment->get('shipment_pickup_remote_id')->value) {
      try {
        $cancelled = $plugin->cancelPickup($pickup_remote_id);
        if (!$cancelled) {
          throw new ShippingLabelGenerationException('Failed to cancel pickup.');
        }
        $shipment->set('shipment_pickup_remote_id', NULL)
          ->set('shipment_pickup_confirmation', NULL)
          ->set('shipment_pickup_status', ScheduledPickup::STATUS_CANCELED)
          ->save();
      }
      catch (\Exception | \Error $e) {
        throw new ShippingLabelGenerationException($e->getMessage(), $e->getCode(), $e);
      }
    }
    return $cancelled;
  }

}
