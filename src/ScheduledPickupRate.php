<?php

namespace Drupal\commerce_shipping_label;
use Drupal\commerce_price\Price;

class ScheduledPickupRate {

  protected ?string $remoteId;

  /**
   * @var array $data
   */
  protected array $data;

  protected ?Price $price;

  protected ?\DateTime $pickupDate;

  protected ?string $description;

  public function getData(): array {
    return $this->data;
  }

  public function setData(array $data): ScheduledPickupRate {
    $this->data = $data;
    return $this;
  }

  public function setDataValue($name, $value) : ScheduledPickupRate {
    $this->data[$name] = $value;
    return $this;
  }

  public function getDataValue($name) {
    return $this->data[$name] ?? NULL;
  }

  public function getRemoteId(): ?string {
    return $this->remoteId;
  }

  public function setRemoteId(?string $remoteId): ScheduledPickupRate {
    $this->remoteId = $remoteId;
    return $this;
  }

  public function getPrice(): ?Price {
    return $this->price;
  }

  public function setPrice(?Price $price): ScheduledPickupRate {
    $this->price = $price;
    return $this;
  }

  public function getPickupDate(): ?\DateTime {
    return $this->pickupDate;
  }

  public function setPickupDate(?\DateTime $pickupDate): ScheduledPickupRate {
    $this->pickupDate = $pickupDate;
    return $this;
  }

  public function getDescription(): ?string {
    return $this->description;
  }

  public function setDescription(?string $description): ScheduledPickupRate {
    $this->description = $description;
    return $this;
  }



}

