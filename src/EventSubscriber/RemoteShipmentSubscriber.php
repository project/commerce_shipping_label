<?php

namespace Drupal\commerce_shipping_label\EventSubscriber;

use Drupal\commerce_shipping_label\Event\RemoteShipmentEvent;
use Drupal\commerce_shipping_label\Event\ShippingLabelEvents;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class RemoteShipmentSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      ShippingLabelEvents::REMOTE_SHIPMENT_CREATED => ['saveRemoteInformationToShipment', 2000],
      ShippingLabelEvents::REMOTE_SHIPMENT_CANCELLED => ['deleteRemoteInformationFromShipment', 2000],
    ];

    return $events;
  }

  public function saveRemoteInformationToShipment(RemoteShipmentEvent $event) {
    $remote_shipment = $event->getRemoteShipment();
    $shipment = $event->getShipment();
    if ($remote_shipment !== NULL) {
      $shipment->set('shipment_label_remote_id', $remote_shipment->getId());
      if (!empty($remote_shipment->getTrackingNumber())) {
        $shipment->setTrackingCode($remote_shipment->getTrackingNumber());
      }
      if ($shipment->getState()->getId() === 'draft') {
        $shipment->set('state', 'ready');
      }
      $shipment->save();
    }
  }

  public function deleteRemoteInformationFromShipment(RemoteShipmentEvent $event) {
    $shipment = $event->getShipment();
    if ($shipment !== NULL) {
      $shipment->setTrackingCode(NULL)
        ->set('shipment_label_remote_id', NULL)
        ->set('state', 'canceled')
        ->save();
    }
  }

}
