<?php

namespace Drupal\commerce_shipping_label\EventSubscriber;

use Drupal\commerce_shipping_label\Event\ShippingLabelEvent;
use Drupal\commerce_shipping_label\Event\ShippingLabelEvents;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class ShippingLabelImportSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;


  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * ShippingLabelImportSubscriber constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      ShippingLabelEvents::SHIPMENT_LABEL_IMPORTED => ['saveFilesToShipment', 2000],
      ShippingLabelEvents::SHIPMENT_LABEL_DELETED => ['deleteFilesFromShipment', 2000],
    ];

    return $events;
  }

  public function saveFilesToShipment(ShippingLabelEvent $event) {
    $remote_label_files = $event->getFiles();
    $shipment = $event->getShipment();
    $files = [];
    $directory = 'private://shipment-label';
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    foreach ($remote_label_files as $remote_label_file) {
      $filename = $this->fileSystem->basename($remote_label_file->getUri());
      $data = file_get_contents($remote_label_file->getUri());
      $file = file_save_data($data, $directory . DIRECTORY_SEPARATOR . $filename);
      $files[] = [
        'target_id' => $file->id(),
        'description' => $remote_label_file->getDescription(),
      ];
    }
    $shipment->set('shipment_label', $files)
      ->save();
  }

  public function deleteFilesFromShipment(ShippingLabelEvent $event) {
    $shipment = $event->getShipment();
    $files = $shipment->get('shipment_label')->referencedEntities();
    foreach ($files as $file) {
      $file->delete();
    }
    $shipment->set('shipment_label', [])
      ->save();
  }

}
