<?php

namespace Drupal\commerce_shipping_label\EventSubscriber;

use Drupal\commerce_shipping_label\Event\ShippingLabelEvents;
use Drupal\commerce_shipping_label\Event\ShippingLabelListEvent;
use Drupal\commerce_shipping_label\ShippingLabelManager;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class ShippingLabelListSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\commerce_shipping_label\ShippingLabelManager
   */
  protected $shippingLabelManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $viewBuilder;

  /**
   * ShippingLabelListSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ShippingLabelManager $shippingLabelManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('commerce_shipment');
    $this->shippingLabelManager = $shippingLabelManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      ShippingLabelEvents::SHIPMENT_LIST_BUILDER_HEADER => [['updateHeader', 100], ['updateHeaderForPickup', -100]],
      ShippingLabelEvents::SHIPMENT_LIST_BUILDER_ROW => [['updateRow', 100], ['updateRowForPickup', -100]],
      ShippingLabelEvents::SHIPMENT_LIST_BUILDER_OPERATIONS => ['updateOperations', 100],
    ];

    return $events;
  }

  public function updateOperations(ShippingLabelListEvent $event) {
    $shipment = $event->getShipment();
    $operations = $event->getData();
    if ($this->shippingLabelManager->supportsRemoteShipments($shipment)) {
      if (!$this->shippingLabelManager->hasRemoteId($shipment)) {
        $operations['commerce_shipping_label_create_remote_shipment'] = [
          'title' => $this->t('Generate shipping labels'),
          'weight' => 10,
          'url' => $shipment->toUrl('generate-shipping-labels'),
        ];
      }
      else {
        $operations['commerce_shipping_label_cancel_remote_shipment'] = [
          'title' => $this->t('Cancel shipping labels'),
          'weight' => 12,
          'url' => $shipment->toUrl('cancel-shipping-labels'),
        ];

        if ($this->shippingLabelManager->supportsSchedulingPickups($shipment)) {
          if (empty($shipment->get('shipment_pickup_remote_id')->value)) {
            $operations['commerce_shipping_label_schedule_pickup'] = [
              'title' => $this->t('Schedule pickup'),
              'weight' => 15,
              'url' => $shipment->toUrl('schedule-pickup'),
            ];
          }
          else {
            $operations['commerce_shipping_label_cancel_pickup'] = [
              'title' => $this->t('Cancel pickup'),
              'weight' => 15,
              'url' => $shipment->toUrl('cancel-pickup'),
            ];
          }
        }


      }
    }
    $event->setData($operations);
  }


  public function updateHeader(ShippingLabelListEvent $event) {
    $header = $event->getData();
    $header = array_slice($header, 0, count($header) - 1, TRUE) +
      ['shipment_label' => $this->t('Labels')] +
      array_slice($header, count($header) - 1, 1, TRUE);
    $event->setData($header);
  }

  public function updateHeaderForPickup(ShippingLabelListEvent $event) {
    $header = $event->getData();
    $header = array_slice($header, 0, count($header) - 1, TRUE) +
      ['pickup' => $this->t('Pickup')] +
      array_slice($header, count($header) - 1, 1, TRUE);
    $event->setData($header);
  }

  public function updateRow(ShippingLabelListEvent $event) {
    $shipment = $event->getShipment();
    $row = $event->getData();
    $label = $this->viewBuilder->viewField($shipment->get('shipment_label'), ['label' => 'hidden']);
    $pickup_info = [];
    $pickup_info[] = ucwords($shipment->get('shipment_pickup_status')->value);
    $pickup_info[] = $shipment->get('shipment_pickup_confirmation')->value;
    $row = array_slice($row, 0, count($row) - 1, TRUE) +
      ['shipment_label' => render($label)] +
      array_slice($row, count($row) - 1, 1, TRUE);
    $event->setData($row);
  }

  public function updateRowForPickup(ShippingLabelListEvent $event) {
    $shipment = $event->getShipment();
    $row = $event->getData();
    $pickup_info = [];
    $pickup_info[] = Html::escape(ucwords($shipment->get('shipment_pickup_status')->value));
    $pickup_info[] = Html::escape($shipment->get('shipment_pickup_confirmation')->value);
    $row = array_slice($row, 0, count($row) - 1, TRUE) +
      ['pickup' => Markup::create(implode('<br />', $pickup_info))] +
      array_slice($row, count($row) - 1, 1, TRUE);
    $event->setData($row);
  }

}
