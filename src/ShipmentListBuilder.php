<?php

namespace Drupal\commerce_shipping_label;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_shipping_label\Event\ShippingLabelListEvent;
use Drupal\commerce_shipping_label\Event\ShippingLabelEvents;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_shipping\ShipmentListBuilder as DefaultShipmentListBuilder;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the list builder for shipments.
 */
class ShipmentListBuilder extends DefaultShipmentListBuilder {

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->setEventDispatcher($container->get('event_dispatcher'));
    return $instance;
  }

  /**
   * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  public function getEventDispatcher(): EventDispatcherInterface {
    return $this->eventDispatcher;
  }

  /**
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *
   * @return ShipmentListBuilder
   */
  public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): ShipmentListBuilder {
    $this->eventDispatcher = $eventDispatcher;
    return $this;
  }

  public function render() {
    $event = new ShippingLabelListEvent(NULL, parent::render());
    $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LIST_BUILDER_RENDER, $event);
    return $event->getData();
  }


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $event = new ShippingLabelListEvent(NULL, parent::buildHeader());
    $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LIST_BUILDER_HEADER, $event);
    return $event->getData();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $event = new ShippingLabelListEvent($entity, parent::buildRow($entity));
    $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LIST_BUILDER_ROW, $event);
    return $event->getData();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $event = new ShippingLabelListEvent($entity, parent::getDefaultOperations($entity));
    $this->eventDispatcher->dispatch(ShippingLabelEvents::SHIPMENT_LIST_BUILDER_OPERATIONS, $event);
    return $event->getData();
  }

}
