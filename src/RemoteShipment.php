<?php

namespace Drupal\commerce_shipping_label;

class RemoteShipment {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $trackingNumber;

  /**
   * RemoteShipment constructor.
   *
   * @param string|null $id
   * @param string|null $trackingNumber
   */
  public function __construct(?string $id = NULL, ?string $trackingNumber = NULL) {
    $this->id = $id;
    $this->trackingNumber = $trackingNumber;
  }

  /**
   * @return string
   */
  public function getId(): ?string {
    return $this->id;
  }

  /**
   * @param string $id
   *
   * @return RemoteShipment
   */
  public function setId(?string $id): RemoteShipment {
    $this->id = $id;
    return $this;
  }

  /**
   * @return string
   */
  public function getTrackingNumber(): ?string {
    return $this->trackingNumber;
  }

  /**
   * @param string $trackingNumber
   *
   * @return RemoteShipment
   */
  public function setTrackingNumber(?string $trackingNumber): RemoteShipment {
    $this->trackingNumber = $trackingNumber;
    return $this;
  }

}
