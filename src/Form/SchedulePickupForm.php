<?php

namespace Drupal\commerce_shipping_label\Form;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping_label\ScheduledPickup;
use Drupal\commerce_shipping_label\SupportsSchedulingPickup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SchedulePickupForm extends FormBase {

  protected CurrencyFormatterInterface $currencyFormatter;


  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->currencyFormatter = $container->get('commerce_price.currency_formatter');
    return $instance;
  }

  public function getFormId() {
    return 'commerce_schedule_pickup_schedule_pickup_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, OrderInterface $commerce_order = NULL, ShipmentInterface $commerce_shipment = NULL) {
    if ($commerce_order === NULL || $commerce_shipment === NULL) {
      return $form;
    }
    $shipping_method = $commerce_shipment->getShippingMethod();
    if ($shipping_method === NULL || !(($plugin = $shipping_method->getPlugin()) && $plugin instanceof SupportsSchedulingPickup)) {
      return $form;
    }

    $form['shipment_id'] = [
      '#type' => 'value',
      '#value' => $commerce_shipment->id(),
    ];

    $build_info = $form_state->getBuildInfo();
    if (!empty($build_info['pickup'])) {
      /** @var \Drupal\commerce_shipping_label\ScheduledPickup $pickup */
      $pickup = $build_info['pickup'];
      $pickup_rates = $pickup->getRates();
      $rate_options = [];
      $default_rate = NULL;
      foreach ($pickup_rates as $rate) {
        $rate_options[$rate->getRemoteId()] = $rate->getDescription() . ' - ' . $this->currencyFormatter->format($rate->getPrice()->getNumber(), $rate->getPrice()->getCurrencyCode());
        if ($default_rate === NULL) {
          $default_rate = $rate->getRemoteId();
        }
      }
      $form['rates_wrapper'] = [
        '#tree' => FALSE,
        '#type' => 'fieldset',
        '#weight' => 90,
        '#title' => $this->t('Pickup Rates'),
      ];
      $form['rates_wrapper']['rate'] = [
        '#type' => 'radios',
        '#options' => $rate_options,
        '#default_value' => $default_rate,
        '#required' => TRUE,
      ];
      $form['pickup'] = [
        '#type' => 'value',
        '#value' => $pickup,
      ];
    }


    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 100,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => empty($build_info['pickup']) ? $this->t('Get Pickup Rates') : $this->t('Schedule Pickup'),
      '#button_type' => 'primary',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('entity.commerce_shipment.collection', [
        'commerce_order' => $commerce_order->id(),
      ]),
    ];

    $form = $plugin->buildPickupSchedulingForm($form, $form_state, $commerce_shipment);
    if (!empty($build_info['pickup'])) {
      foreach (Element::children($form) as $key) {
        if (!in_array($key, ['rates_wrapper', 'actions'])) {
          $form[$key]['#access'] = FALSE;
        }
      }
    }

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $shipment_id = $form_state->getValue('shipment_id');
    /** @var ShipmentInterface $shipment */
    $shipment = \Drupal::entityTypeManager()->getStorage('commerce_shipment')->load($shipment_id);

    if ($shipment === NULL) {
      return;
    }

    $shipping_method = $shipment->getShippingMethod();
    if ($shipping_method === NULL || !(($plugin = $shipping_method->getPlugin()) && $plugin instanceof SupportsSchedulingPickup)) {
      return;
    }

    /** @var \Drupal\commerce_shipping_label\ScheduledPickup $pickup */
    $pickup = $form_state->getValue('pickup');
    if ($pickup !== NULL) {
      $pickup = $plugin->buildScheduledPickupFromFormSubmit($form, $form_state, $pickup);
      $selected_pickup_rate = $form_state->getValue('rate');
      foreach ($pickup->getRates() as $rate) {
        if ($rate->getRemoteId() === $selected_pickup_rate) {
          $pickup = $plugin->schedulePickup($pickup, $rate);
          break;
        }
      }
      if ($pickup->getStatus() === ScheduledPickup::STATUS_SCHEDULED) {
        $shipment->set('shipment_pickup_remote_id', $pickup->getRemoteId())
          ->set('shipment_pickup_confirmation', $pickup->getConfirmationNumber())
          ->set('shipment_pickup_status', $pickup->getStatus())
          ->save();
        $this->messenger()->addStatus($this->t('Pickup scheduled. The confirmation number is @confirmation_number.', [
          '@confirmation_number' => $pickup->getConfirmationNumber(),
        ]));
        $form_state->setRedirect('entity.commerce_shipment.collection', [
          'commerce_order' => $shipment->getOrderId(),
        ]);
      }
      else {
        $build_info = $form_state->getBuildInfo();
        if (isset($build_info['pickup'])) {
          unset($build_info['pickup']);
        }
        $form_state->setBuildInfo($build_info);
        $form_state->setRebuild(TRUE);
      }
      return;
    }


    $pickup = new ScheduledPickup($shipment);
    $pickup = $plugin->buildScheduledPickupFromFormSubmit($form, $form_state, $pickup);
    $rates = $plugin->getPickupRates($pickup);
    $pickup->setRates($rates);
    $build_info = $form_state->getBuildInfo();
    $build_info['pickup'] = $pickup;
    $form_state->setBuildInfo($build_info);
    $form_state->setRebuild(TRUE);
  }

}
