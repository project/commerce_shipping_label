<?php

namespace Drupal\commerce_shipping_label;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Form\FormStateInterface;

interface SupportsSchedulingPickup {

  public function buildPickupSchedulingForm(array $form, FormStateInterface $formState, ShipmentInterface $shipment): array;

  public function validatePickupSchedulingForm(array $form, FormStateInterface $formState, ShipmentInterface $shipment): void;

  public function buildScheduledPickupFromFormSubmit(array $form, FormStateInterface $formState, ScheduledPickup $pickup): ScheduledPickup;

  public function schedulePickup(ScheduledPickup $pickup, ScheduledPickupRate $selectedRate): ScheduledPickup;

  public function getPickupRates(ScheduledPickup $pickup) : array;

  public function cancelPickup(string $pickup_remote_id) : bool;

}
