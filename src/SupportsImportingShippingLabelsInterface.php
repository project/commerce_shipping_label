<?php

namespace Drupal\commerce_shipping_label;

use Drupal\commerce_shipping\Entity\ShipmentInterface;

interface SupportsImportingShippingLabelsInterface {

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \Drupal\commerce_shipping_label\RemoteLabelFile[]
   */
  public function getRemoteLabelFiles(ShipmentInterface $shipment) : array;

}
