<?php

namespace Drupal\commerce_shipping_label;

class RemoteLabelFile {

  /**
   * @var string
   */
  protected $uri;

  /**
   * @var string
   */
  protected $description;

  /**
   * RemoteLabelFile constructor.
   *
   * @param string $uri
   * @param string|null $description
   */
  public function __construct(string $uri, string $description = NULL) {
    $this->uri = $uri;
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getUri(): string {
    return $this->uri;
  }

  /**
   * @param string $uri
   *
   * @return RemoteLabelFile
   */
  public function setUri(string $uri): RemoteLabelFile {
    $this->uri = $uri;
    return $this;
  }

  /**
   * @return string
   */
  public function getDescription(): ?string {
    return $this->description;
  }

  /**
   * @param string|null $description
   *
   * @return RemoteLabelFile
   */
  public function setDescription(?string $description): RemoteLabelFile {
    $this->description = $description;
    return $this;
  }

}
