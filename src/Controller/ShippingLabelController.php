<?php

namespace Drupal\commerce_shipping_label\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping_label\ShippingLabelGenerationException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ShippingLabelController extends ControllerBase {

  /**
   * @var \Drupal\commerce_shipping_label\ShippingLabelManager
   */
  protected $shippingLabelManager;

  /**
   * ShippingLabelController constructor.
   *
   * @param \Drupal\commerce_shipping_label\ShippingLabelManager $shippingLabelManager
   */
  public function __construct(\Drupal\commerce_shipping_label\ShippingLabelManager $shippingLabelManager) {
    $this->shippingLabelManager = $shippingLabelManager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\commerce_shipping_label\Controller\ShippingLabelController|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_shipping_label.manager')
    );
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function generateLabels(OrderInterface $commerce_order, ShipmentInterface $commerce_shipment) {
    try {
      $this->shippingLabelManager->createRemoteShipment($commerce_shipment);
      $this->shippingLabelManager->importLabelFiles($commerce_shipment);
      $this->messenger()->addStatus($this->t('Generated shipping labels for @label.', ['@label' => $commerce_shipment->label()]));
    }
    catch (ShippingLabelGenerationException $e) {
      $this->messenger()->addError($e->getMessage());
    }

    return new RedirectResponse(
      Url::fromRoute('entity.commerce_shipment.collection', ['commerce_order' => $commerce_order->id()])->toString()
    );
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function cancelLabels(OrderInterface $commerce_order, ShipmentInterface $commerce_shipment) {
    try {
      $this->shippingLabelManager->cancelRemoteShipment($commerce_shipment);
      $this->shippingLabelManager->deleteLabelFiles($commerce_shipment);
      $this->messenger()->addStatus($this->t('Cancelled shipping labels for @label.', ['@label' => $commerce_shipment->label()]));
    }
    catch (ShippingLabelGenerationException $e) {
      $this->messenger()->addError($e->getMessage());
    }

    return new RedirectResponse(
      Url::fromRoute('entity.commerce_shipment.collection', ['commerce_order' => $commerce_order->id()])->toString()
    );
  }

  public function cancelPickup(OrderInterface $commerce_order, ShipmentInterface $commerce_shipment) {
    try {
      $this->shippingLabelManager->cancelPickup($commerce_shipment);
      $this->messenger()->addStatus($this->t('Cancelled pickup for @confirmation.', [
        '@confirmation' => $commerce_shipment->label()
      ]));
    }
    catch (ShippingLabelGenerationException $e) {
      $this->messenger()->addError($e->getMessage());
    }
    return new RedirectResponse(
      Url::fromRoute('entity.commerce_shipment.collection', ['commerce_order' => $commerce_order->id()])->toString()
    );
  }
}
