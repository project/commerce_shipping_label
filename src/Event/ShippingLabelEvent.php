<?php

namespace Drupal\commerce_shipping_label\Event;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping_label\RemoteShipment;
use Symfony\Component\EventDispatcher\Event;

/**
 * @see \Drupal\commerce_shipping_label\Event\ShippingLabelEvents
 */
class ShippingLabelEvent extends Event {

  /**
   * The shipment.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * @var \Drupal\commerce_shipping_label\RemoteLabelFile[]
   */
  protected $files;

  /**
   * RemoteShipmentEvent constructor.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   * @param \Drupal\commerce_shipping_label\RemoteLabelFile[]|null $files
   */
  public function __construct(ShipmentInterface $shipment, ?array $files = []) {
    $this->shipment = $shipment;
    $this->files = $files;
  }

  /**
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

  /**
   * @return \Drupal\commerce_shipping_label\RemoteLabelFile[]
   */
  public function getFiles(): ?array {
    return $this->files;
  }

  /**
   * @param \Drupal\commerce_shipping_label\RemoteLabelFile[] $files
   *
   * @return ShippingLabelEvent
   */
  public function setFiles(?array $files = []): ShippingLabelEvent {
    $this->files = $files;
    return $this;
  }

}
