<?php

namespace Drupal\commerce_shipping_label\Event;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping_label\RemoteShipment;
use Symfony\Component\EventDispatcher\Event;

/**
 * @see \Drupal\commerce_shipping_label\Event\ShippingLabelEvents
 */
class RemoteShipmentEvent extends Event {

  /**
   * The shipment.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * @var \Drupal\commerce_shipping_label\RemoteShipment
   */
  protected $remoteShipment;

  /**
   * RemoteShipmentEvent constructor.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   * @param \Drupal\commerce_shipping_label\RemoteShipment|null $remoteShipment
   */
  public function __construct(ShipmentInterface $shipment, ?RemoteShipment $remoteShipment = NULL) {
    $this->shipment = $shipment;
    $this->remoteShipment = $remoteShipment;
  }

  /**
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

  /**
   * @return \Drupal\commerce_shipping_label\RemoteShipment
   */
  public function getRemoteShipment(): ?RemoteShipment {
    return $this->remoteShipment;
  }

}
