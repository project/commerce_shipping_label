<?php
namespace Drupal\commerce_shipping_label\Event;

class ShippingLabelEvents {

  /**
   * @Event
   *
   * @see \Drupal\commerce_shipping_label\Event\ShippingLabelListEvent
   */
  public const SHIPMENT_LIST_BUILDER_HEADER = 'commerce_shipping_label.list_builder_header';

  /**
   * @Event
   *
   * @see \Drupal\commerce_shipping_label\Event\ShippingLabelListEvent
   */
  public const SHIPMENT_LIST_BUILDER_ROW = 'commerce_shipping_label.list_builder_row';

  /**
   * @Event
   *
   * @see \Drupal\commerce_shipping_label\Event\ShippingLabelListEvent
   */
  public const SHIPMENT_LIST_BUILDER_OPERATIONS = 'commerce_shipping_label.list_builder_operations';

  /**
   * @Event
   *
   * @see \Drupal\commerce_shipping_label\Event\ShippingLabelListEvent
   */
  public const SHIPMENT_LIST_BUILDER_RENDER = 'commerce_shipping_label.list_builder_render';

  public const REMOTE_SHIPMENT_PRECREATE = 'commerce_shipping_label.remote_shipment.pre_create';

  public const REMOTE_SHIPMENT_CREATED = 'commerce_shipping_label.remote_shipment.created';

  public const SHIPMENT_LABEL_PREIMPORT = 'commerce_shipping_label.label_import.pre_import';

  public const SHIPMENT_LABEL_IMPORTED = 'commerce_shipping_label.label_import.pre_import';

  public const SHIPMENT_LABEL_PREDELETE = 'commerce_shipping_label.label_import.pre_delete';

  public const SHIPMENT_LABEL_DELETED = 'commerce_shipping_label.label_import.deleted';

  public const REMOTE_SHIPMENT_PRECANCEL = 'commerce_shipping_label.remote_shipment.pre_cancel';

  public const REMOTE_SHIPMENT_CANCELLED = 'commerce_shipping_label.remote_shipment.cancelled';

}
