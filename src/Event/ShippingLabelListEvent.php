<?php

namespace Drupal\commerce_shipping_label\Event;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * @see \Drupal\commerce_shipping_label\Event\ShippingLabelEvents
 */
class ShippingLabelListEvent extends Event {

  /**
   * The shipment.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * @var array
   */
  protected $data;

  /**
   * Constructs a new FilterShippingMethodsEvent object.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface|null $shipment
   *   The shipment.
   * @param array $data
   */
  public function __construct(?ShipmentInterface $shipment = NULL, array $data = []) {
    $this->shipment = $shipment;
    $this->data = $data;
  }

  /**
   * Gets the shipment.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment.
   */
  public function getShipment() {
    return $this->shipment;
  }

  /**
   * @return array
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * @param array $data
   *
   * @return ShippingLabelListEvent
   */
  public function setData(array $data): ShippingLabelListEvent {
    $this->data = $data;
    return $this;
  }

}
