(function ($, Drupal, drupalSettings) {
  "use strict";

  Drupal.commerce_shipping_label_zebra_browser_print = {};
  Drupal.commerce_shipping_label_zebra_browser_print.selected_device = null;
  Drupal.commerce_shipping_label_zebra_browser_print.devices = [];

  Drupal.behaviors.browserPrintShippingList = {
    attach: function (context, settings) {
      $(context).find('form.commerce-shipping-label-zebraprint-form').once('browser-print').each(function() {
        var button = $(this).find('.button--primary').first();
        var selector = $(this).find('select').first();
        var url = $(button).attr('data-url');
        //Get the default device from the application as a first step. Discovery takes longer to complete.
        BrowserPrint.getDefaultDevice("printer", function(device)
        {
          //Add device to list of devices and to html select element
          Drupal.commerce_shipping_label_zebra_browser_print.selected_device = device;
          Drupal.commerce_shipping_label_zebra_browser_print.devices.push(device);
          var option = $('<option value="' + device.uid + '">' + device.name + '</option>');
          $(selector).append(option);

          //Discover any other devices available to the application
          BrowserPrint.getLocalDevices(function(device_list) {
            for(var i = 0; i < device_list.length; i++)
            {
              //Add device to list of devices and to html select element
              var device = device_list[i];
              if(!Drupal.commerce_shipping_label_zebra_browser_print.selected_device
                || device.uid != Drupal.commerce_shipping_label_zebra_browser_print.selected_device.uid) {
                Drupal.commerce_shipping_label_zebra_browser_print.devices.push(device);
                var option = $('<option value="' + device.uid + '">' + device.name + '</option>');
                $(selector).append(option);
              }

            }
            $(selector).bind('change', function() {
              Drupal.commerce_shipping_label_zebra_browser_print.devices.forEach(function(device) {
                if (device.uid == $(selector).val()) {
                  Drupal.commerce_shipping_label_zebra_browser_print.selected_device = device;
                }
              });
            });
            if (Drupal.commerce_shipping_label_zebra_browser_print.devices.length > 0) {
              $(button).bind('click', function(e) {
                Drupal.commerce_shipping_label_zebra_browser_print.selected_device.sendFile(url, undefined, function(errorMessage){
                  alert("Print Error: " + errorMessage);
                });
                e.stopPropagation();
                return false;
              });
              $(button).removeAttr('disabled').removeClass('is-disabled');
            }
          }, function(){
            console.log("Error getting local devices")
          }, "printer");
        }, function(error){
          console.log(error);
        });
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
