<?php

namespace Drupal\commerce_shipping_label_zebra\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class PrintForm extends FormBase {

  public function getFormId() {
    return 'commerce_shipping_label_zebra.print_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $url = NULL) {
    $form['print_form'] = [
      '#type' => 'container',
      '#attributes' => [
        //'class' => 'invisible',
      ],
    ];
    $form['print_form']['printer'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Printer'),
      '#options' => [],
    ];

    $form['print_form']['actions'] = [
      '#type' => 'actions',
    ];
    $form['print_form']['actions']['print'] = [
      '#type' => 'button',
      '#button_type' => 'primary',
      '#value' => $this->t('Print'),
      '#disabled' => TRUE,
      '#attributes' => [
        'data-url' => $url,
      ],
    ];
    $form['#attached']['library'] = 'commerce_shipping_label_zebra/browser-print';
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Not used
  }


}
