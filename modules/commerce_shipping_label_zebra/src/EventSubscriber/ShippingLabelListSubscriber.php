<?php

namespace Drupal\commerce_shipping_label_zebra\EventSubscriber;

use Drupal\commerce_shipping_label\Event\ShippingLabelEvents;
use Drupal\commerce_shipping_label\Event\ShippingLabelListEvent;
use Drupal\commerce_shipping_label\ShippingLabelManager;
use Drupal\commerce_shipping_label_zebra\Form\PrintForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class ShippingLabelListSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   */
  public function __construct(\Drupal\Core\Form\FormBuilderInterface $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      ShippingLabelEvents::SHIPMENT_LIST_BUILDER_HEADER => ['updateHeader', 75],
      ShippingLabelEvents::SHIPMENT_LIST_BUILDER_ROW => ['updateRow', 75],
    ];

    return $events;
  }

  public function updateHeader(ShippingLabelListEvent $event) {
    $header = $event->getData();
    $header = array_slice($header, 0, count($header) - 1, TRUE) +
      ['print' => $this->t('Print')] +
      array_slice($header, count($header) - 1, 1, TRUE);
    $event->setData($header);
  }

  public function updateRow(ShippingLabelListEvent $event) {
    $shipment = $event->getShipment();
    $row = $event->getData();
    $url = NULL;
    if ($shipment->hasField('shipment_label') && !$shipment->get('shipment_label')->isEmpty()) {
      /** @var \Drupal\file\FileInterface[] $files */
      $files = $shipment->get('shipment_label')->referencedEntities();
      foreach ($files as $file) {
        if (preg_match('/\.zpl$/', $file->getFilename())) {
          $url = file_create_url($file->getFileUri());
          break;
        }
      }
    }
    if ($url !== NULL) {
      $form = $this->formBuilder->getForm(PrintForm::class, $url);
      $row = array_slice($row, 0, count($row) - 1, TRUE) +
        ['print' => render($form)] +
        array_slice($row, count($row) - 1, 1, TRUE);
    }
    else {
      $row = array_slice($row, 0, count($row) - 1, TRUE) +
        ['print' => ''] +
        array_slice($row, count($row) - 1, 1, TRUE);
    }
    $event->setData($row);
  }

}
